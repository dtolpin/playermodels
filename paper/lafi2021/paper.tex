\documentclass[acmsmall,review,screen]{acmart}

\usepackage[utf8]{inputenc}
\usepackage{setspace}
\usepackage{microtype}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{booktabs}
\usepackage{url}
\usepackage{float}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{xcolor}

\bibliographystyle{ACM-Reference-Format}
\citestyle{acmauthoryear}

\lstset{%
    basicstyle       = \ttfamily\small,
    keywordstyle     = \bfseries,
    stringstyle      = \color{gray},
    commentstyle     = \itshape\color{gray},
    showstringspaces = false,
    numbers          = left,
}

%\doublespacing

\newcommand{\var}[1]{{\operatorname{\mathit{#1}}}}

\newtheorem{dfn}{Definition}
\newtheorem{cnj}{Conjecture}

\renewcommand*{\ttdefault}{pcr}

\floatstyle{ruled}
\floatname{model}{Model}
\newfloat{model}{H}{}

\floatstyle{ruled}
\floatname{listing}{Listing}
\newfloat{listing}{ht}{}

\begin{document}

\acmConference[LAFI '22]{LAFI '22: The Seventh International Workshop on Languages for Inference}{Sun 16 Jan, 2022}{ Philadelphia, Pennsylvania, United States}
\acmBooktitle{LAFI '22}
\acmPrice{}
\acmISBN{}


\acmJournal{PACMPL}
\acmVolume{}
\acmNumber{LAFI} % CONF = POPL or ICFP or OOPSLA
\acmArticle{3}
\acmYear{2022}
\acmMonth{1}
\acmDOI{} % \acmDOI{10.1145/nnnnnnn.nnnnnnn}
\startPage{1}
\setcopyright{none}



\title{Bob and Alice Go to a Bar}
\subtitle{Reasoning About Future With Probabilistic Programs}
\author{David Tolpin}
\email{david.tolpin@gmail.com}
\author{Tomer Dobkin}
\email{tomer.dobkin@gmail.com}
\affiliation{%
  \institution{Ben-Gurion University of the Negev}
  \city{Beer Sheva}
  \country{Israel}
}

\begin{abstract} 
It is well known that reinforcement learning can be cast as
inference in an appropriate probabilistic model. However, this
commonly involves introducing a distribution over agent
trajectories with probabilities proportional to exponentiated
rewards. In this work, we formulate reinforcement learning as
Bayesian inference without resorting to rewards, and show that
rewards are derived from agent's preferences, rather than the
other way around. We argue that agent preferences should be
specified stochastically rather than deterministically.
Reinforcement learning via inference with stochastic preferences
naturally describes agent behaviors, does not require
introducing rewards and exponential weighing of trajectories,
and allows to reason about agents using the solid foundation of
Bayesian statistics. Stochastic conditioning, a probabilistic
programming paradigm for conditioning models on distributions
rather than values, is the formalism behind agents with
probabilistic preferences.  We demonstrate realization of our
approach on case studies using both a two-agent coordination game
and a single agent acting in a noisy environment, showing that
despite superficial differences, both cases can be modelled and
reasoned about based on the same principles.
\end{abstract}

\maketitle

\section{Introduction}

The `planning as inference' paradigm~\cite{WGR+11,MPT+16}
extends Bayesian inference to future observations. The agent in
the environment is modelled as a Bayesian generative model, but
the belief about the distribution of agent's actions is updated
based on future goals rather than on past facts.  This allows to
use common modelling and inference tools, notably
\textit{probabilistic programming}, to represent computer agents
and explore their behavior.  Representing agents as general
programs provides flexibility compared to restricted approaches,
such as Markov decision processes and their variants and
extensions, and allows to model a broad range of complex
behaviors in a unified and natural way.

Planning, or, more generally, reinforcement learning, as
inference models agent preferences through conditioning agents
on preferred future behaviors.  Often, the conditioning is
achieved through the Boltzmann distribution: the probability of
a realization of agent's behavior is proportional to the
exponent of the agent's expected reward. The motivation of using
the Boltzmann distribution is not clear though. A `rational'
agent should behave in a way that maximizes the agent's expected
utility, shouldn't it? One argument is that the Boltzmann
distribution models human errors and irrationality. Sometimes,
attempts are made to avoid using Boltzmann distribution by
explicitly conditioning the agent on future
goals~\cite{SG14,ESS+17,SMW18}.  However, such conditioning can
also lead to irrational behavior, as the famous Newcomb's
paradox~\cite{N69} suggests.

\section{The fable: Bob and Alice go to a bar}

Challenges of reinforcement learning as inference can be
illustrated on the following fable~\cite{SG14}:

\begin{quote}
Bob and Alice want to meet in a bar, but Bob left his phone at
home. There are two bars which Bob and Alice visit with
known different frequencies. Which bar Bob and Alice should head
if they want to meet?
\end{quote}

Many different settings can be considered based on this story.
Bob and Alice may know each other's preferences with respect to
the bars and to the meeting with the other person, be uncertain
about the preferences, or hold wrong beliefs about the
preferences.  Their preferences may be collaborative (both want to
meet) or adversarial (Bob wants to meet Alice, but Alice avoids
Bob).  Bob may consider Alice's deliberation about Bob's
behavior, and vice versa, recursively. It turns out that all
these scenarios can be represented by a single model of
interacting agents.  However, doing this properly, from the
viewpoint of both specification and inference, requires certain
care. Multi-agent planning as inference, exemplified by the
fable about Bob and Alice, presents logical and statistical
traps which are easy to fall into, often without even noticing.
One common mistake is conditioning on a future event as on a
present observation, which is related to the Newcomb's paradox;
however there are also other mistakes. 


\section{Probabilistic preferences}
\label{sec:probpref}

The fable of Bob and Alice is underspecified: we know how strong
Bob or Alice prefer the first bar over the second one, or vice
versa, but not how strong Bob and Alice prefer to meet (or
avoid) each other. We need a consistent language for expressing
both preferences. One common way of expressing preferences is by
ascribing rewards to action outcomes~\cite{ESS+17}. In this way,
the agent's preferences are specified using three rewards: for
visiting each of the bars and for meeting the other agent.

To reconcile rewards and stochastic action
choice, the softmax agent was proposed and is broadly used for
modelling `approximately optimal agents'~\cite{SG14}.
However, one may wonder what would be the optimal behavior if
the behavior we model with softmax is only `approximately
optimal' and hence suboptimal. We believe that treating
stochastic behavior as suboptimal because it does not
maximize the utility based on rewards we ascribe is
contradictory. When Alice chooses the first bar 55\% of
time, it is her choice to go for a drink and sometimes have a
lot of people around and a loud music, sometimes have fewer
people around and read a book without disturbing music in the
background.

Instead of devising rewards that match the probabilities, we
can and should specify agents' preferences directly using
probabilities.  According to the fable, the optimal probability
of Alice's choice with respect to the bars is given (observed
with high confidence or proclaimed by an oracle). To complete
the formulation, it remains to specify the optimal probability
of Alice to meet Bob.  It turns out that we need to specify the
probability of Alice choosing to visit the bar where Bob is
heading, provided  Alice knows Bob's plans and would otherwise
visit either bar with equal probability.

Such model, in a superficially confusing way, would generate
Bob's location based on Alice's desire to meet Bob. However, the
confusion can be resolved by the following interpretation: when
Alice chooses to go to the first bar, it is because, in addition
to the slight preference over the second bar, she believes that
Bob will also go to the first bar, with a certain probability.
It is Alice's anticipations of Bob's behavior that are generated
by the model, rather than events.

Since the model generates \textit{anticipations}, it should also
be conditioned on anticipations.  Let us accept as a fact that
Alice concludes that Bob goes to the first bar with probability
$p_1^{Bob}$. Then, the model must be \textbf{stochastically
conditioned}~\cite{TZR+21} on Bob's visit to the first bar being distributed
as $\mathrm{Bernoulli}(p_1^{Bob})$: 
\begin{model}
    \caption{The Alice's Model}\label{mod:detpref-gen-stocond}
    \begin{algorithmic}
        \State $\mathbb{I}_{Bob}^1 \sim \mathrm{Bernoulli}(\hat p_{Bob}^1)$
        \State \rule{0.5\linewidth}{0.5pt}
        \State $\mathbb{I}_{Alice}^1 \sim \mathrm{Bernoulli}(p_{Alice}^1)$
        \If {$\mathbb{I}_{Alice}^1$}
            \State $\mathbb{I}_{Bob}^1 \sim \mathrm{Bernoulli}(p_{Alice}^m)$
        \Else
            \State $\mathbb{I}_{Bob}^1 \sim \mathrm{Bernoulli}(1 - p_{Alice}^m)$
        \EndIf
    \end{algorithmic}
\end{model}

\section{Numerical experiments}

Probabilistic preferences and stochastic conditioning enable
exploration of various modes of interaction between agents
entirely within the framework of probabilistic programming and
Bayesian inference, using general purpose inference algorithms.
Figure~\ref{fig:bob-and-alice} illustrates some of the
interactions using a Gen~\cite{CSL+19} implementation of the
fable.

\begin{figure}[ht]
    \begin{subfigure}{0.245\linewidth}
		\centering
        \includegraphics[width=0.9\linewidth]{ba-meet.pdf}
        \caption{Bob and Alice want to meet}
    \end{subfigure}
    \begin{subfigure}{0.245\linewidth}
		\centering
        \includegraphics[width=0.9\linewidth]{ba-avoid.pdf}
        \caption{Bob and Alice avoid each other}
    \end{subfigure}
    \begin{subfigure}{0.245\linewidth}
		\centering
        \includegraphics[width=0.9\linewidth]{ba-chase-mild.pdf}
    \caption{Bob chases Alice, mild feelings}
    \end{subfigure}
    \begin{subfigure}{0.245\linewidth}
		\centering
        \includegraphics[width=0.9\linewidth]{ba-chase-strong.pdf}
		\caption{Bob chases Alice, strong feelings}
    \end{subfigure}
	\caption{Interactions of Bob and Alice}
    \label{fig:bob-and-alice}
\end{figure}

\nocite{TD21}

\bibliography{refs}

\end{document}
