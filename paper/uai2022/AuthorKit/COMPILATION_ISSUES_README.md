This file collects information useful for people encountering issues while
using the `uai2022.cls` file.

* _Please use an up-to-date TeX installation_, such as TeXlive 2019 or 2020.
  TeXlive 2018 and earlier are known to contain incompatible LaTeX packages.
  Specifically, the `\section` command causes an error.

* For your submission pdf, the big patch of white space between the title
  and abstract is normal. That is where the author block will appear for
  accepted papers (cf. `uai2022-template-submission.pdf` and
  `uai2022-template-accepted.pdf`).

* An earlier version of the class could trigger a warning when adding a
  reference inside a sectioning command (e.g., `\section{… \ref{…}`). This has
  been fixed by loading the `textcase` package in the class.

* In case you load both the `amsthm` and `tikz-external` packages, a weird
  interaction with the `hyperref` and `lastpage` packages loaded by the class
  can occur. A solution is to also load the `cleverref` package! For more
  information, see <https://tex.stackexchange.com/questions/538748>.
