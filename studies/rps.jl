### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ de008ea7-41fe-49d9-a3bd-6441af84d0db
using Gen, StatsFuns, StatsPlots

# ╔═╡ 3837ad02-d343-11eb-085c-d7a4677b7cad
md"# Rock-Paper-Scissors 

Experimenting with policy learning and game playing on rock-paper-scissors (RPS). RPS is two-player episodic zero-sum game. In each episode, both players simultaneously choose one of three options:

* rock
* paper 
* scissors

There is a cyclic non-transitive ordering: rock < paper < scissors < rock. If both players choose the same item, it is a draw. Otherwise the player who chose a 'greater' item according to the ordering wins.

An optimal strategy for both players is to choose an item randomly and uniformly, thus achieving a draw. We assume that
1. each player has a Bayes in item choices, 
2. players can engage in multiple episodes to learn each others biases, and improve the chances to win accordingly.
"

# ╔═╡ 54b1d62f-d6ba-43a4-81cb-a34a99a0f0d1
begin
	ROCK = 1
	PAPER = 2
	SCISSORS = 3
	
	"returns the reward of player a"
	function reward(choice_a, choice_b)
		reward = 0
		if choice_a != choice_b 
			reward = 1
			if choice_a < choice_b 
				choice_a, choice_b = choice_b, choice_a
				reward = -reward
			end
			if choice_a == SCISSORS && choice_b == ROCK
				reward = -reward
			end
		end
		reward
	end
	
	@assert reward(ROCK, ROCK)==0
	@assert reward(ROCK, PAPER)==-1
	@assert reward(PAPER, ROCK)==1
	@assert reward(ROCK, SCISSORS)==1
	@assert reward(PAPER, SCISSORS)==-1
end

# ╔═╡ 911b39e3-8597-452f-b1e5-6bf0e9fe78be
md"## Learning the bias from game history"

# ╔═╡ d8f1dd9a-ed7f-4114-bfe1-686cce911850
md"We are going to draw the choice probabilities from the Dirichlet distribution. Gen does not have Dirichlet in the standard library, so we just sample from Gamma and normalize.
"

# ╔═╡ 958e3e1e-9ff3-44b2-950a-1c657e2a66dd
"draw preference vector (Gen does not have Dirichlet)"
@gen function pref(alpha)
	pref = Vector{Float64}(undef, length(alpha))
	for i = eachindex(alpha)
		pref[i] = @trace(gamma(alpha[i], 1), i)
	end
	return pref ./ sum(pref)
end;

# ╔═╡ dd4f6815-4243-416b-809b-3914f2fe6b76
md"We can now define a static game simulator, in which the preferences are supposed to be fixed.
"

# ╔═╡ 098b0c03-4a23-422d-aa27-4b5ff48d4e74
@gen function static_game(n, gamma=1, alpha_a=[1,1,1], alpha_b=[1,1,1])
	pref_a = @trace(pref(alpha_a), :pref_a)
	pref_b = @trace(pref(alpha_b), :pref_b)
	for i = 1:n
		@trace(categorical(pref_a), (:choice_a, n-i+1))
		@trace(categorical(pref_b), (:choice_b, n-i+1))
	end
	choice_a = @trace(categorical(pref_a), (:choice_a, 0))
	choice_b = @trace(categorical(pref_b), (:choice_b, 0))
	score = reward(choice_a, choice_b)
	p_a = logistic(gamma*score)
	@trace(bernoulli(p_a), :win_a)
	return score
end;

# ╔═╡ 0fd65735-eec4-425e-9a2d-c87d9140b071
md"Let's do some inference on this basic model"

# ╔═╡ 6b40e13d-4d33-40c6-b38b-b7613024cc1d
let scores = []
	Pq
end

# ╔═╡ c771acf8-86c2-44a2-8b92-3c80856ad45f
md"The inference appears to do something reasonable, but there are several problems with the approach.

1. When we condition on a to win, we also condition on b to lose. That is, we learn the preferences of a and b so that a wins and b loses.
2. We assume that the preferences of a and b have been fixed during the whole history.

Instead, we want to update the preferences of a to maximize the chances to win given b plays according to their own best strategy. Additionally, we should assume that both players adjust their preferences after every move.
"

# ╔═╡ 83f04299-a425-4ab1-8373-0d7fb5336f25
md"## Markov agents

Let's try to fix these problems. In general, players' preferences are affected

* by the full history of moves;
* by their reasoning about the best move.

Let's assume that the agents are Markovian: the influence of the past history of moves is fully summarized by their current preferences, and the preferences are updated after every move. Then, the game consists of two transitions:

1. Agents make moves based on their current preferences. 
2. Agents updates preferences based on the moves.
"

# ╔═╡ 3cf5a5e5-0d1b-4add-8477-4b73857eddc5
@gen function markov_game(n, m_a=1, m_b=1, gamma=1, alpha_a=[1,1,1], alpha_b=[1,1,1])
	# Last preferences (m is the stubbornness of each player)
	for i = 1:m_a
		@trace(pref(alpha_a), (:pref_a_last, i))
	end
	for i = 1:m_b
		@trace(pref(alpha_b), (:prev_b_last, i))
	end
	
	# Current preferences
	pref_a = @trace(pref(alpha_a), :pref_a)
	pref_b = @trace(pref(alpha_b), :pref_b)

	# Choices (TODO: theory of mind)
	choice_a = @trace(categorical(pref_a), (:choice_a, 0))
	choice_b = @trace(categorical(pref_b), (:choice_b, 0))
	
	score = reward(choice_a, choice_b)
	p_a = logistic(gamma*score)
	@trace(bernoulli(p_a), :win_a)
	
	return score
end;

# ╔═╡ 95b56e56-0f00-477b-bf87-e79381709559
md"By conditioning on different variables, we can both update the preferences based on the observed moves, and select moves based on the current preferences. By alternating these two steps, we play the game.
"

# ╔═╡ Cell order:
# ╠═de008ea7-41fe-49d9-a3bd-6441af84d0db
# ╟─3837ad02-d343-11eb-085c-d7a4677b7cad
# ╠═54b1d62f-d6ba-43a4-81cb-a34a99a0f0d1
# ╟─911b39e3-8597-452f-b1e5-6bf0e9fe78be
# ╟─d8f1dd9a-ed7f-4114-bfe1-686cce911850
# ╠═958e3e1e-9ff3-44b2-950a-1c657e2a66dd
# ╟─dd4f6815-4243-416b-809b-3914f2fe6b76
# ╠═098b0c03-4a23-422d-aa27-4b5ff48d4e74
# ╟─0fd65735-eec4-425e-9a2d-c87d9140b071
# ╠═6b40e13d-4d33-40c6-b38b-b7613024cc1d
# ╟─c771acf8-86c2-44a2-8b92-3c80856ad45f
# ╟─83f04299-a425-4ab1-8373-0d7fb5336f25
# ╠═3cf5a5e5-0d1b-4add-8477-4b73857eddc5
# ╟─95b56e56-0f00-477b-bf87-e79381709559
