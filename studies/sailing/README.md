# The sailing problem

Run:

* `make` to build, 
* `make run` to run the experiment,
* `jupyter notebook posterior.ipynb` to open the posterior analysis notebook.
