package main

import (
	"math"
	"math/rand"
)

const (
	nDir = 8

	// tacks
	port      = -1
	starboard = 1

	// move costs
	delayCost = 4.0
	awayCost  = 1.0
	downCost  = 2.0
	crossCost = 3.0
	upCost    = 4.0
	intoCost  = math.MaxFloat64

	// wind distribution, the next wind is either
	// in the same or an adjacent direction
	pSameWind = 0.4
)

type pos [2]float64

var (
	// x y
	dirs = [nDir][2]float64{
		{+1, +0}, {+1, +1}, {+0, +1}, {-1, +1},
		{-1, +0}, {-1, -1}, {+0, -1}, {+1, -1},
	}

	// l w -> tack
	tacks [nDir][nDir]int

	// l w -> cost
	costs [nDir][nDir]float64

	// p(dirs[j]|dirs[i]) = pWind[i][j]
	pWind [nDir][nDir]float64
)

// cross product, used to initialize tacks
func xprod(a, b pos) float64 {
	return a[0]*b[1] - a[1]*b[0]
}

// scalar product
func sprod(a, b pos) float64 {
	return a[0]*b[0] + a[1]*b[1]
}

// Euclidian norm
func norm(a pos) float64 {
	return math.Sqrt(sprod(a, a))
}

// Euclidian distance
func dist(a, b pos) float64 {
	var c pos
	c[0] = a[0] - b[0]
	c[1] = a[1] - b[1]
	return norm(c)
}

// cosinus product
func cprod(a, b pos) float64 {
	return sprod(a, b) / (norm(a) * norm(b))
}

func init() {
	// initialize the tacks
	for l := 0; l != nDir; l++ {
		for w := 0; w != nDir; w++ {
			x := xprod(dirs[l], dirs[w])
			var tack int
			switch {
			case x < 0:
				tack = port
			case x > 0:
				tack = starboard
			default:
				tack = 0
			}
			tacks[l][w] = tack
		}
	}

	// initialize costs
	for i := 0; i != nDir; i++ {
		for j := 0; j != nDir; j++ {
			c := cprod(dirs[i], dirs[j])
			var cost float64
			switch {
			case c > 0.9:
				cost = awayCost
			case c > 0.5:
				cost = downCost
			case c > -0.5:
				cost = crossCost
			case c > -0.9:
				cost = upCost
			default:
				cost = intoCost
			}
			costs[i][j] = cost
		}
	}

	// initialize wind probabilities
	for i := 0; i != nDir; i++ {
		for j := 0; j != nDir; j++ {
			c := cprod(dirs[i], dirs[j])
			var p float64
			switch {
			case c > 0.9:
				p = pSameWind
			case c > 0.5:
				p = 0.5 * (1 - pSameWind)
			}
			pWind[i][j] = p
		}
	}
}

func oppositeTacks(ta, tb int) bool {
	switch ta {
	case 1:
		return tb == -1
	case -1:
		return tb == 1
	default:
		return false
	}
}

// Computes move cost
func legCost(t, l, w int) float64 {
	cost := 0.
	// tack change cost
	if oppositeTacks(t, tacks[l][w]) {
		cost += delayCost
	}
	// traversal cost
	cost += norm(dirs[l]) * costs[l][w]
	return cost
}

// Samples the next wind
func nextWind(last int) int {
	r := rand.Float64()
	for next := 0; next != nDir; next++ {
		r -= pWind[last][next]
		if r < 0.0 {
			return next
		}
	}
	return -1 // cannot happen
}

type winds []int

// lazily compute winds
func (winds *winds) at(s int) int {
	for len(*winds) <= s {
		var next int
		if len(*winds) == 0 {
			next = rand.Intn(nDir)
		} else {
			last := winds.at(len(*winds) - 1)
			next = nextWind(last)
		}
		*winds = append(*winds, next)
	}

	return (*winds)[s]
}

type policy interface {
	leg(size int, p pos, t, w int) int
}

func traverse(size int, policy policy, winds *winds) (
	cost float64,
	trajectory []pos,
) {
	p := pos{0., 0.}
	trajectory = append(trajectory, p)
	t, s := 0, 0
	for int(p[0]) != size-1 || int(p[1]) != size-1 {
		w := winds.at(s)
		l := policy.leg(size, p, t, w)
		for i := 0; i != 2; i++ {
			p[i] += dirs[l][i]
			if int(p[i]) == -1 || int(p[i]) == size {
				cost += intoCost
			}
		}
		cost += legCost(t, l, w)
		t = tacks[l][w]
		s++
		trajectory = append(trajectory, p)
	}

	return cost, trajectory
}
