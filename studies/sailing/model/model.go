package model

type ParticleModel interface {
	Observe(x []float64, p interface{}) float64
	NewParticle() interface{}
}
